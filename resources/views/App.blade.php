<!DOCTYPE html>
<html lang="en">
<head>
    <title>MyLead - zadanie rekrutacyjne - @yield('title')</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow" style="margin-bottom:25px;">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="./"><small>Zarządzanie Produktami</small></a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <form action="{{ route('products.index') }}" method="GET" role="search" style="width: 75%">
        <input class="form-control" name="search" type="text" placeholder="Szukaj produktów" aria-label="Szukaj"
               style="width:75%"/>
    </form>
    <ul class="navbar-nav px-3">
        @auth
            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <li class="nav-item text-nowrap">
                    <input type="submit" style="background: none; color: white;" value="{{ __('Wyloguj') }}"/>
                </li>
            </form>
        @else

            <li class="nav-item text-nowrap">
                <a class="nav-link" href="login/" style="color: white;">{{ __('Zaloguj') }}</a>
            </li>
        @endauth
    </ul>
</header>
@include('Sidebar')
@yield('content')

<script>
    $('#flash-overlay-modal').modal();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
        integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
        integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>
<script src="{{ asset('js/dashboard.js') }}"></script>
</body>
</html>
