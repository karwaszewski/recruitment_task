@extends('App')
@section('title', 'Lista produktów')

@section('content')
    <div class="content col-md-9 ms-sm-auto col-lg-10 px-md-4">

        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1 class="h2">@yield('title')</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group me-2">
                    @auth
                        <a href="{{ URL::to('products/create') }}">
                            <button type="button" class="btn btn-success">Dodaj</button>
                        </a>
                    @endauth

                </div>
            </div>
        </div>
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

                <form action="{{ route('products.index') }}" method="GET" style="width: 75%">
                    <select class="form-select" aria-label="Wybierz sortowanie" name="sort_by">
                        <option value="" selected>Sortowanie</option>
                        <option value="name">Po nazwie</option>
                        <option value="id">W kolejności utworzenia</option>
                    </select>
                    <select class="form-select" aria-label="Kierunek sortowania" name="sort_order">
                        <option value="" selected>Kierunek</option>
                        <option value="descend">rosnąco</option>
                        <option value="ascend">malejąco</option>
                    </select>
                    <input type="submit" value="Filtruj" class="btn btn-primary h-full mb-1"/>
                </form>
        </div>
        {{ $products->links() }}
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th>Suma cen</th>
                    @auth
                        <th>Akcja</th>@endauth
                </tr>
                </thead>

                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>
                            <a href="{{ URL::to('products/'.$product->id) }}">{{ $product->name }}</a>
                        </td>
                        <td>
                            {{ count($product->price) }}
                        </td>
                        @auth
                            <td>

                                <a href="{{ route('products.edit', ['product' => $product->id]) }}">
                                    <button type="button" class="btn btn-sm btn-success">Edytuj
                                    </button>
                                </a>
                                <form action="{{ route('products.destroy', ['product' => $product->id]) }}"
                                      method="POST"
                                      style="display: inline;">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <input type="submit" value="Usuń" name="Usuń" class="btn btn-sm btn-danger"
                                           accesskey="x"/>
                                </form>
                            </td>
                        @endauth
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $products->links() }}
        </div>
        </main>
@endsection
