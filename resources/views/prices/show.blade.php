@extends('App')
@section('title', 'Zobacz ceny')

@section('content')
    <main class="content col-md-9 ms-sm-auto col-lg-10 px-md-4">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1 class="h2">@yield('title')</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group me-2">
                    <a href="{{ route('prices.index') }}">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Powrót do listy</button>
                    </a>&nbsp;
                    @auth
                        <a href="{{ route('prices.edit', ['price' => $price->id]) }}">
                            <button type="button" class="btn btn-sm btn-success">Edytuj
                            </button>
                        </a>
                    @endauth                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h1 class="mb-12">Produkt: {{ $price->product->name }}</h1>

            <form action="{{ route('prices.destroy', ['price' => $price->id]) }}" method="POST"
                  style="display: inline;">
                @csrf
                <input type="hidden" name="_method" value="DELETE"/>
                <button type="submit" class="btn btn-primary">
                    {{ $price->name }}: <span class="badge badge-light">{{ $price->value }}</span> <i
                        class="fas fa-minus-circle"></i>
                </button>
            </form>

    </main>
@endsection
