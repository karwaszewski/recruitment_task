@extends('App')
@section('title', 'Lista cen')

@section('content')
    <main class="content col-md-9 ms-sm-auto col-lg-10 px-md-4">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1 class="h2">@yield('title')</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group me-2">
                    @auth
                        <a href="{{ URL::to('prices/create') }}">
                            <button type="button" class="btn btn-success">Dodaj</button>
                        </a>
                    @endauth
                </div>
            </div>
        </div>
        {{ $prices->links() }}
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>Cena</th>
                    <th>Wartość</th>
                    <th>Produkt</th>
                    @auth
                        <th>Akcja</th>@endauth
                </tr>
                </thead>

                <tbody>
                @foreach($prices as $price)
                    <tr>
                        <td>
                            <a href="{{ URL::to('prices/'.$price->id) }}">{{ $price->name }}</a>
                        </td>
                        <td>{{ $price->value }} zł</td>
                        <td><a href="{{ URL::to('products/'.$price->product->id) }}">{{ $price->product->name }}</a>
                        </td>
                        @auth
                            <td>
                                <a href="{{ route('prices.edit', ['price' => $price->id]) }}">
                                    <button type="button" class="btn btn-sm btn-success">Edytuj
                                    </button>
                                </a>
                                <form action="{{ route('prices.destroy', ['price' => $price->id]) }}" method="POST"
                                      style="display: inline;">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <input type="submit" value="Usuń" name="Usuń" class="btn btn-sm btn-danger"
                                           accesskey="x"/>
                                </form>
                            </td>
                        @endauth
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $prices->links() }}
        </div>
    </main>
@endsection
