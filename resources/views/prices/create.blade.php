@extends('App')
@section('title', 'Dodaj cenę')

@section('content')
    <main class="content col-md-9 ms-sm-auto col-lg-10 px-md-4">
        @auth
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1 class="h2">@yield('title')</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group me-2">
                    <a href="{{ route('prices.index') }}"><button type="button" class="btn btn-sm btn-outline-secondary">Powrót do listy</button></a>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <form action="{{ route('prices.store') }}" method="POST" >
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="value">Nazwa</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="value">Wartość</label>
                    <input type="text" class="form-control" id="value" name="value">
                </div>
                <div class="form-group">
                    <label for="product_id">Produkt</label>
                    <select class="form-control" id="product_id" name="product_id">
                        @foreach($products as $product)
                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Zapisz</button>
            </div>
        </div>

    </form>

        @else
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Restricted Area!
            </div>
        @endauth
    </main>
    </main>

@endsection
