<?php

namespace App\Policies;

use App\Models\Product;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ProductPolicy
{
    use HandlesAuthorization;

    public function viewAny(): bool
    {
        return true;
    }

    public function create(): bool
    {
        if (Auth::check()) {
            return true;
        } else {
            return false;
        }
    }

    public function edit(): bool
    {
        if (Auth::check()) {
            return true;
        } else {
            return false;
        }
    }

    public function update(): bool
    {
        if (Auth::check()) {
            return true;
        } else {
            return false;
        }
    }

    public function delete(): bool
    {
        if (Auth::check()) {
            return true;
        } else {
            return false;
        }
    }

}
