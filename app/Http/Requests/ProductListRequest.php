<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductListRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'name' => ['string', 'max:255', 'nullable'],
        ];
    }
    public function getOrderByInput(): ?string
    {
        return $this->input('sort_by', null);
    }

    public function getDirectionInput(): string
    {
        $sortOrder = $this->input('sort_order');
        if ($sortOrder === 'ascend') {
            return 'ASC';
        }
        if ($sortOrder === 'descend') {
            return 'DESC';
        }
        return 'ASC';
    }

    public function getSearchInput(): ?string
    {
        return $this->input('search');
    }


}
