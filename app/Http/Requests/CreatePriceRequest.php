<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePriceRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'name' => ['required', 'max:255'],
            'value' => ['required'],
            'product_id' => ['required', 'integer']
        ];

    }

    public function getValueInput(): float {
        return str_replace(",",".",$this->input('value'));
    }

    public function getProductIdInput(): int
    {
        return $this->input('product_id');
    }

    public function getNameInput(): string
    {
        return $this->input('name');
    }
}
