<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductListRequest;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;
use App\Traits\UploadTrait;
use Illuminate\Database\Eloquent\Builder;


class ProductController extends Controller
{
    use UploadTrait;

    public function index(ProductListRequest $request): View
    {
        $products = Product::query()
            ->when(!empty($request->getSearchInput()), function (Builder $query) use ($request) {
                $query->where(Product::COLUMN_NAME, 'LIKE', '%'.$request->getSearchInput().'%');
            })
            ->when(
                $request->getOrderByInput(),
                function (Builder $query) use ($request) {
                    return $query->orderBy($request->getOrderByInput(), $request->getDirectionInput());
                }
            )
            ->paginate(25);
        return view('products.index', compact('products'));
    }

    public function create(): View
    {
            return view('products.create');
    }

    public function store(Request $request): RedirectResponse
    {

        $request->validate([
            'name' => 'required',
            'image' => ['image','mimes:jpeg,png,jpg,gif,svg','max:4096']
        ]);

        if ($request->has('image')) {

            $image = $request->file('image');
            $name = Str::slug($request->input('name')) . '_' . time();
            $folder = '/uploads/images/';
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
        } else {
            $filePath = '';
        }

        $product = new Product([
            Product::COLUMN_NAME => $request->input('name'),
            Product::COLUMN_DESCRIPTION => $request->input('description'),
            Product::COLUMN_IMAGE => $filePath
        ]);

        $product->save();
        return redirect()->route('products.index');
    }

    public function show(Product $product): View
    {
        return view('products.show', compact('product'));
    }

    public function edit(Product $product): View
    {
            return view('products.edit', compact('product'));
    }

    public function update(Request $request, Product $product): RedirectResponse
    {

        $request->validate([
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = Str::slug($request->input('name')) . '_' . time();
            $folder = '/uploads/images/';
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
        } else {
            $filePath = '';
        }

        $product->fill([
            Product::COLUMN_NAME => $request->input('name'),
            Product::COLUMN_DESCRIPTION => $request->input('description'),
            Product::COLUMN_IMAGE => $filePath
        ]);
        $product->save();
        return redirect()->route('products.index');
    }

    public function destroy(Product $product): RedirectResponse
    {
        $product->delete();
        return redirect()->route('products.index');
    }
}
