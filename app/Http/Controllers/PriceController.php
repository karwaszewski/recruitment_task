<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePriceRequest;
use App\Http\Requests\UpdatePriceRequest;
use App\Models\Price;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PriceController extends Controller
{
    public function index(): View
    {
        $prices = Price::query()
            ->with([
                Price::RELATION_PRODUCT
            ])
            ->paginate(25);

        return view('prices.index', compact('prices'));
    }

    public function create(): View
    {
        $products = Product::query()->get();
        return view('prices.create', compact('products'));
    }

    public function store(CreatePriceRequest $request): RedirectResponse
    {
        $price = new Price([
            Price::COLUMN_NAME => $request->getNameInput(),
            Price::COLUMN_PRODUCT_ID => $request->getProductIdInput(),
            Price::COLUMN_VALUE => $request->getValueInput(),
        ]);
        $price->product()->associate($request->getProductIdInput());

        $price->save();

        $price->load(
            Price::RELATION_PRODUCT,
        );

        return redirect()->route('prices.index');
    }

    public function show(Price $price): View
    {
        return view('prices.show', compact('price'));
    }

    public function edit(Price $price): View
    {
        $products = Product::query()->get();
        return view('prices.edit', compact('price', 'products'));
    }

    public function update(UpdatePriceRequest $request, Price $price): RedirectResponse
    {
        $price->fill([
            Price::COLUMN_NAME => $request->getNameInput(),
            Price::COLUMN_PRODUCT_ID => $request->getProductIdInput(),
            Price::COLUMN_VALUE => $request->getValueInput(),
        ]);
        $price->product()->associate($request->getProductIdInput());

        $price->save();

        $price->load(
            Price::RELATION_PRODUCT,
        );

        return redirect()->route('prices.index');
    }

    public function destroy(Price $price): RedirectResponse
    {
        $price->delete();
        return redirect()->route('prices.index');
    }
}
