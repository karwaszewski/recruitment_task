<?php

namespace App\Traits;

trait SortableRequest
{
    public function getOrderByInput(): ?string
    {
        return $this->get('sort_by', null);
    }

    public function getDirectionInput(): string
    {
        $sortOrder = $this->get('sort_order');
        if ($sortOrder === 'ascend') {
            return 'ASC';
        }
        if ($sortOrder === 'descend') {
            return 'DESC';
        }
        return 'ASC';
    }
}
