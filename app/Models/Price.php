<?php

namespace App\Models;

use  Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Price
 *
 * @property integer id
 * @property string name
 * @property float value
 * @property int product_id
 */

class Price extends Model
{
    use HasFactory;

    public const TABLE = 'prices';

    public const COLUMN_ID = 'id';
    public const COLUMN_NAME = 'name';
    public const COLUMN_VALUE = 'value';
    public const COLUMN_PRODUCT_ID = 'product_id';

    public const RELATION_PRODUCT = 'product';

    public $timestamps = true;
    protected $table = self::TABLE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COLUMN_NAME,
        self::COLUMN_VALUE,
        self::COLUMN_PRODUCT_ID,
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, self::COLUMN_PRODUCT_ID);
    }

}
