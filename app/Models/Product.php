<?php

namespace App\Models;

use  Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Product
 *
 * @property integer id
 * @property string name
 * @property string description
 * @property string image_url
 */

class Product extends Model
{
    use HasFactory;

    public const TABLE = 'products';

    public const COLUMN_ID = 'id';
    public const COLUMN_NAME = 'name';
    public const COLUMN_DESCRIPTION = 'description';
    public const COLUMN_IMAGE = 'image';

    public $timestamps = true;
    protected $table = self::TABLE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COLUMN_NAME,
        self::COLUMN_DESCRIPTION,
        self::COLUMN_IMAGE,
    ];

    public function price(): HasMany
    {
        return $this->hasMany(Price::class, Price::COLUMN_PRODUCT_ID);
    }

}
