<?php


namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function list_test()
    {
        $product = Product::factory()->create();
        $response = $this->get('/products');
        $response->assertSee($product->name);
    }

    public function single_test()
    {
        $product = Product::factory()->create();
        $response = $this->get('/products/' . $product->id);
        $response->assertSee($product->name);
    }

    public function create_test()
    {
        $product = Product::factory()->create();
        $this->post('/products/create', $product->toArray());
    }

    public function product_requires_name()
    {
        $product = Product::factory()->create([
            'name' => null
        ]);
        $this->post('/products/create', $product->toArray())
            ->assertSessionHasErrors('name');
    }

    public function delete_test()
    {
        $product = Product::factory()->create();
        $this->delete('/products/' . $product->id);
        $this->assertDatabaseMissing('products', ['id' => $product->id]);

    }
}
